Source: powerline-gitstatus
Section: python
Priority: optional
Maintainer: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>
Uploaders: Jerome Charaoui <jerome@riseup.net>,
           Samuel Henrique <samueloph@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 powerline (>= 2.6),
 python3-all,
 python3-powerline,
 python3-setuptools,
Build-Conflicts:
 powerline-gitstatus,
Rules-Requires-Root: no
Standards-Version: 4.5.0
Homepage: https://github.com/jaspernbrouwer/powerline-gitstatus
Vcs-Browser: https://salsa.debian.org/python-team/applications/powerline-gitstatus
Vcs-Git: https://salsa.debian.org/python-team/applications/powerline-gitstatus.git

Package: powerline-gitstatus
Architecture: all
Depends:
 git,
 powerline ${powerline:Version},
 python3-powerline-gitstatus,
 ${misc:Depends},
Built-Using:
 ${built-using},
Description: Powerline Git segment
 Powerline is a statusline plugin for vim, and provides statuslines and
 prompts for several other applications, including zsh, bash, tmux,
 IPython, Awesome and Qtile.
 .
 This package contains the configuration handling for powerline.

Package: python3-powerline-gitstatus
Architecture: all
Depends:
 python3-powerline ${powerline:Version},
 ${misc:Depends},
 ${python3:Depends},
Built-Using:
 ${built-using},
Description: Powerline Git segment for Python (3.x)
 Powerline is a statusline plugin for vim, and provides statuslines and
 prompts for several other applications, including zsh, bash, tmux,
 IPython, Awesome and Qtile.
 .
 This package contains the Git segment for showing the status of a Git working
 copy for Python 3.x.
